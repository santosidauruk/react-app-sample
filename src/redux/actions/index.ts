import { Dispatch } from 'redux';
import {
  FETCH_ALL_POKEMON_FAILED,
  FETCH_ALL_POKEMON_REQUEST,
  FETCH_ALL_POKEMON_SUCCESS,
  FETCH_FILTERED_POKEMON_FAILED,
  FETCH_FILTERED_POKEMON_REQUEST,
  FETCH_FILTERED_POKEMON_SUCCESS
} from './types';
import getAllPokemon from '../../services/getAllPokemon';
import getPokemonOfType from '../../services/getPokemonOfType';

const fetchAllPokemon = (path: string) => (dispatch: Dispatch) => {
  dispatch({
    type: FETCH_ALL_POKEMON_REQUEST
  })
  return getAllPokemon({
    path
    })
    .then(pokemons => dispatch({
      type: FETCH_ALL_POKEMON_SUCCESS,
      payload: {
        nextUrl: pokemons.next,
        data: pokemons.results
      }
    }))
    .catch(error => dispatch({
      type: FETCH_ALL_POKEMON_FAILED,
      payload: error
    }))
}

const fetchFilteredPokemon = (pokemonType: string) => (dispatch: Dispatch) => {
  dispatch({
    type: FETCH_FILTERED_POKEMON_REQUEST
  })
  return getPokemonOfType({
    param: {
      type: pokemonType
    }
    })
    .then(pokemons => dispatch({
      type: FETCH_FILTERED_POKEMON_SUCCESS,
      payload: {
        pokemonType,
        data: pokemons
      }
    }))
    .catch(error => dispatch({
      type: FETCH_FILTERED_POKEMON_FAILED,
      payload: error
    }))
}


export { fetchAllPokemon, fetchFilteredPokemon }
