import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import mockAxios from 'axios'
import * as type from '../../actions/types';
import * as actions from '../../actions';
// import { initialState as initialStatePokemonList } from '../../reducers/pokemonList';
// import { initialState as initialStateFilteredPokemon } from '../../reducers/filteredPokemonList';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe("test fetchAllPokemon", () => {
  let store
  const fakePayload = {
    nextUrl: '',
    data: []
  }

  beforeEach(() => {
    store = mockStore({})
  })
  afterEach(() => {
    fetchMock.restore()
  })

  it("should FETCH_ALL_POKEMON_REQUEST and FETCH_ALL_POKEMON_SUCCESS when fetch all pokemon done", () => {
    mockAxios.get.mockImplementationOnce(() => Promise.resolve({
      data: {
        next: '',
        results: []
      }
    }))

    const expectedActions = [
      { type: type.FETCH_ALL_POKEMON_REQUEST },
      { type: type.FETCH_ALL_POKEMON_SUCCESS, payload: fakePayload}
    ]

    return store.dispatch(actions.fetchAllPokemon()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it("should FETCH_ALL_POKEMON_REQUEST and FETCH_ALL_POKEMON_FAILED when error happens", () => {
    mockAxios.get.mockImplementationOnce(() => Promise.reject({}))

    const expectedActions = [
      { type: type.FETCH_ALL_POKEMON_REQUEST },
      { type: type.FETCH_ALL_POKEMON_FAILED, payload: {} }
    ]

    return store.dispatch(actions.fetchAllPokemon()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})

describe("test fetchFilteredPokemon", () => {
  let store
  const fakePayload = {
    data: []
  }

  beforeEach(() => {
    store = mockStore({})
  })
  afterEach(() => {
    fetchMock.restore()
  })

  it("should FETCH_FILTERED_POKEMON_REQUEST and FETCH_FILTERED_POKEMON_SUCCESS when fetch types done", () => {
    mockAxios.get.mockImplementationOnce(() => Promise.resolve({
      data: {
        pokemon: []
      }
    }))

    const expectedActions = [
      { type: type.FETCH_FILTERED_POKEMON_REQUEST },
      { type: type.FETCH_FILTERED_POKEMON_SUCCESS, payload: fakePayload}
    ]

    return store.dispatch(actions.fetchFilteredPokemon()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it("should FETCH_FILTERED_POKEMON_REQUEST and FETCH_FILTERED_POKEMON_FAILED when error happens", () => {
    mockAxios.get.mockImplementationOnce(() => Promise.reject({}))

    const expectedActions = [
      { type: type.FETCH_FILTERED_POKEMON_REQUEST },
      { type: type.FETCH_FILTERED_POKEMON_FAILED, payload: {} }
    ]

    return store.dispatch(actions.fetchFilteredPokemon()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
