import { Reducer } from 'redux';
import {
  FETCH_FILTERED_POKEMON_REQUEST,
  FETCH_FILTERED_POKEMON_SUCCESS,
  FETCH_FILTERED_POKEMON_FAILED
} from '../actions/types';

export const initialState = {
  loading: true,
  data: [],
  error: {},
  pokemonType: ''
}

export const filteredPokemonListReducer: Reducer = ( state = initialState, action) => {
  switch (action.type) {
    case FETCH_FILTERED_POKEMON_REQUEST:
      return {
          ...state,
          loading: true
      }
    case FETCH_FILTERED_POKEMON_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        pokemonType: action.payload.pokemonType
      }
    case FETCH_FILTERED_POKEMON_FAILED:
      return {
        ...state,
        loading: true,
        pokemonType: '',
        error: action.payload
      }
    default:
      return state
  }
}
