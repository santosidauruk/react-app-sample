import { combineReducers } from 'redux'
import { pokemonListReducer } from './pokemonList';
import { filteredPokemonListReducer } from './filteredPokemonList';

const rootReducer = combineReducers({
  pokemonList: pokemonListReducer,
  filteredList: filteredPokemonListReducer
})

export default rootReducer
