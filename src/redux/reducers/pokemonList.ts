import { Reducer } from 'redux';
import {
  FETCH_ALL_POKEMON_REQUEST,
  FETCH_ALL_POKEMON_SUCCESS,
  FETCH_ALL_POKEMON_FAILED,
} from '../actions/types';

export const initialState = {
  loading: true,
  data: [],
  error: {},
  nextUrl: '',
}

export const pokemonListReducer: Reducer = ( state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_POKEMON_REQUEST:
      return {
          ...state,
          loading: true
      }
    case FETCH_ALL_POKEMON_SUCCESS:
      return {
        ...state,
        loading: false,
        nextUrl: action.payload.nextUrl,
        data: [...state.data, ...action.payload.data]
      }
    case FETCH_ALL_POKEMON_FAILED:
      return {
        ...state,
        loading: true,
        error: action.payload
      }
    default:
      return state
  }
}
