import { initialState, filteredPokemonListReducer } from '../filteredPokemonList';
import * as types from '../../actions/types';

describe("test reducer", () => {
  it("should return initial state", () => {
    expect(filteredPokemonListReducer(undefined, {})).toEqual(initialState)
  })

  it("should return loading true", () => {
    expect(filteredPokemonListReducer(undefined, {
      type: types.FETCH_FILTERED_POKEMON_REQUEST
    })).toEqual({
      ...initialState,
      loading: true
    })
  })
  it("should handle action filter pokemon failed", () => {
    expect(filteredPokemonListReducer(undefined, {
      type: types.FETCH_FILTERED_POKEMON_FAILED,
      payload: {}
    })).toEqual({
      ...initialState,
      loading: true,
      error: {},
    })
  })
  it("should handle add filter pokemon success", () => {
    expect(filteredPokemonListReducer(undefined, {
      type: types.FETCH_FILTERED_POKEMON_SUCCESS,
      payload: {
        data: [],
        pokemonType: ''
      }
    })).toEqual({
      ...initialState,
      loading: false,
      data: [],
      pokemonType: ''
    })
  })
})
