import { initialState, pokemonListReducer } from '../pokemonList';
import * as types from '../../actions/types';

describe("test reducer", () => {
  it("should return initial state", () => {
    expect(pokemonListReducer(undefined, {})).toEqual(initialState)
  })

  it("should return loading true", () => {
    expect(pokemonListReducer(undefined, {
      type: types.FETCH_ALL_POKEMON_REQUEST
    })).toEqual({
      ...initialState,
      loading: true
    })
  })
  it("should handle action filter pokemon failed", () => {
    expect(pokemonListReducer(undefined, {
      type: types.FETCH_ALL_POKEMON_FAILED,
      payload: {}
    })).toEqual({
      ...initialState,
      loading: true,
      error: {},
    })
  })
  it("should handle add filter pokemon success", () => {
    expect(pokemonListReducer(undefined, {
      type: types.FETCH_ALL_POKEMON_SUCCESS,
      payload: {
        nextUrl: '',
        data: []
      }
    })).toEqual({
      ...initialState,
      loading: false,
      nextUrl: '',
      data: []
    })
  })
})
