import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers/rootReducer'

const development = process.env.NODE_ENV !== 'production'

const composeMiddleware = development
  ? composeWithDevTools(applyMiddleware(thunkMiddleware))
  : applyMiddleware(thunkMiddleware)

const configureStore = (initialState = {} ) =>
  createStore(rootReducer, initialState, composeMiddleware)

export default configureStore
