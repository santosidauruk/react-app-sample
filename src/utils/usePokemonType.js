import { useState, useEffect } from 'react';
import getType from '../services/getType';

const usePokemonType = () => {
  const [types, setTypes] = useState([])

  useEffect(() => {
    getType()
      .then(types => {
        const dropdownTypes = types.map(type => ({
          key: type.name,
          value: type.name,
          text: type.name.charAt(0).toUpperCase() + type.name.slice(1)
        }))
        setTypes(dropdownTypes)
      })
      .catch(error => { throw error })
  },[])

  return types
};

export default usePokemonType;
