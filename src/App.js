import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route } from 'react-router-dom';
import { Home, Detail, Filter } from './pages';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact><Home/></Route>
        <Route path="/pokemon/:name" exact><Detail/></Route>
        <Route path="/pokemon/type/:pokemonType" exact><Filter/></Route>
      </Switch>
    </Router>
  );
}

export default App;
