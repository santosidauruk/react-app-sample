import React from 'react';
import { shallow } from 'enzyme'
import PokeDetail from "../PokeDetail"
import PokeMiniCard from "../PokeMiniCard"


describe("test render each component", () => {
  it("PokeDetail should render correctly", () => {
    const wrapper = shallow(<PokeDetail detail={{
      sprites: [],
      abilities: [],
      types: []
    }}/>)
    expect(wrapper).toMatchSnapshot();
  })
  it("PokeMiniCard should render correctly", () => {
    const wrapper = shallow(<PokeMiniCard pokemon={{}}/>)
    expect(wrapper).toMatchSnapshot();
  })
})
