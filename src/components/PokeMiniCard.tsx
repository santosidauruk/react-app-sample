import React from 'react';
import { Card, Button } from 'semantic-ui-react';
import { PokemonItem } from '../interfaces';

interface Props {
  pokemon: PokemonItem,
  style: object,
  index: number
}

const PokeMiniCard: React.FC<Props> = ({ pokemon, style, index }) => {
  return (
    <Card key={pokemon.name} centered style={style}>
      <Card.Content>
        <Card.Header style={{
          marginTop: 0
        }}>{pokemon.name}</Card.Header>
      </Card.Content>
      <Card.Content extra>
          <Button basic color='green' size="mini" floated="right">
            See Details
          </Button>
      </Card.Content>
    </Card>
  );
};

export default PokeMiniCard;
