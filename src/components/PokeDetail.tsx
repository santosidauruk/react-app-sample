import React, { Fragment } from 'react';
import { Tab, Image, Menu } from 'semantic-ui-react';
import { PokemonDetail, Ability, PokemonDetailType} from '../interfaces';

interface Props {
  detail: PokemonDetail
}

const PokeDetail: React.FC<Props> = ({ detail }) => {
  const { sprites, abilities, types } = detail

  const imagePanes = Object.keys(sprites)
                      .filter(key => sprites[key] !== null && typeof sprites[key] === 'string')
                      .map((key) => ({
                        menuItem: <Menu.Item key={sprites[key]}><img src={sprites[key]} alt={sprites[key]}/></Menu.Item>,
                        pane: <Tab.Pane key={sprites[key]} attached="top"><Image size="small" src={sprites[key]}/></Tab.Pane>
                      }))

  return (
    <div style={{ padding: '16px' }}>
      <Tab menu={{
        attached: "bottom",
        secondary: true
      }} panes={imagePanes} renderActiveOnly={false}/>
      <AbilityList abilities={abilities}/>
      <br/>
      <TypeList types={types}/>
    </div>
  );
};

const AbilityList: React.FC<{ abilities: Array<Ability> }> = ({ abilities }) => (
  <Fragment>
    <b>Abilities: </b>
      {
        abilities.map((ability, index) => (
          <Fragment key={ability.ability.name}>
            <span> {ability.ability.name}</span>
            { index < abilities.length - 1 && <span>,</span> }
          </Fragment>
        ))
      }
  </Fragment>
)

const TypeList: React.FC<{ types: Array<PokemonDetailType> }> = ({ types }) => (
  <Fragment>
    <b>Types: </b>
      {
        types.map((type, index) => (
          <Fragment key={type.type.name}>
            <span> {type.type.name}</span>
            { index < types.length - 1 && <span>,</span> }
          </Fragment>
        ))
      }
  </Fragment>
)

export default PokeDetail;
