export interface PokemonItem {
  name: string,
  url: string
}

export interface PokemonTypeItem {
  pokemon: PokemonItem,
  slot: number
}

export interface Ability {
  ability: {
    name: string,
    url: string
  },
  is_hidden: boolean,
  slot: number
}

type Sprites = {
  [key: string]: string
}

export interface PokemonDetailType {
  slot: number,
  type: PokemonItem
}

export interface PokemonDetail {
  abilities: Array<Ability>,
  sprites: Sprites,
  types: Array<PokemonDetailType>
}

export interface PokemonListState {
  loading: boolean,
  data: Array<PokemonItem>,
  error: Error,
  nextUrl: string
}

export interface FilteredPokemonListState {
  loading: boolean,
  data: Array<PokemonTypeItem>,
  error: Error,
  pokemonType: string
}

export interface RootState {
  pokemonList: PokemonListState,
  filteredList: FilteredPokemonListState
}

export interface Pokemons {
  results: Array<PokemonItem>,
  next: string
}
