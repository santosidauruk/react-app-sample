import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store'
import { shallow } from 'enzyme'
import thunk from 'redux-thunk';
import { MemoryRouter } from "react-router-dom";
import { Detail, Filter, Home } from '../index';

const mockStore = configureMockStore([thunk])

describe("test render each page", () => {
  let store
  beforeEach(() => {
    store = mockStore({})
  })

  const ComponentWrapper = ({ component: Component}) => (
    <Provider store={store}>
        <MemoryRouter>
          <Component/>
        </MemoryRouter>
      </Provider>
  )
  it("homepage should render correctly", () => {
    const wrapper = shallow(<ComponentWrapper component={Home} />)
    expect(wrapper).toMatchSnapshot();
  })

  it("detail page should render correctly", () => {
    const wrapper = shallow(<ComponentWrapper component={Detail} />)
    expect(wrapper).toMatchSnapshot();
  })

  it("filter page should render correctly", () => {
    const wrapper = shallow(<ComponentWrapper component={Filter} />)
    expect(wrapper).toMatchSnapshot();
  })
})
