import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FixedSizeList } from 'react-window';
import InfiniteLoader from 'react-window-infinite-loader';
import { Loader, Dropdown } from 'semantic-ui-react';
import { Link, useHistory } from 'react-router-dom';
import { fetchAllPokemon} from '../redux/actions';
import PokeMiniCard from '../components/PokeMiniCard';
import usePokemonType from '../utils/usePokemonType';
import { RootState } from '../interfaces';

const Home: React.FC = () => {
  const pokemons = useSelector((state: RootState) => state.pokemonList)
  const types = usePokemonType()
  const dispatch = useDispatch()
  const history = useHistory()

  const isItemLoaded = (index: number): boolean => !pokemons.nextUrl || index < pokemons.data.length;

  useEffect(() => {
    if(!pokemons.nextUrl){
      dispatch(fetchAllPokemon(pokemons.nextUrl ? pokemons.nextUrl : "/pokemon"))
    }
  }, [dispatch, pokemons.nextUrl])

  const ListItem: React.FC<{index: number, style: {}}> = ({ index, style }) => {
    if(!isItemLoaded(index)) {
      return <div style={style}><Loader active inline="centered"/></div>
    }
    return (
      <div style={style}>
        <Link to={`/pokemon/${pokemons.data[index].name}`}>
          <PokeMiniCard index={index} pokemon={pokemons.data[index]} style={{
            marginBottom: "15px"
          }}/>
        </Link>
      </div>
    )
  }

  return (
      <div>
        <Dropdown
          style={{
            width: '200px',
            margin: '0 auto 40px auto'
          }}
          placeholder='Select Type'
          fluid
          search
          selection
          options={types}
          onChange={(_, data) => history.push(`/pokemon/type/${data.value}`)}
        />
        {
          pokemons.data.length < 20
          ? <Loader active inline="centered"/>
          : <InfiniteLoader
                itemCount={pokemons.nextUrl ? pokemons.data.length + 1 : pokemons.data.length}
                //@ts-ignore
                loadMoreItems={() => dispatch(fetchAllPokemon(pokemons.nextUrl))}
                isItemLoaded={isItemLoaded}
                >
                {({ onItemsRendered, ref }) => {
                  return <FixedSizeList
                    height={500}
                    width="100%"
                    onItemsRendered={onItemsRendered}
                    itemCount={pokemons.nextUrl ? pokemons.data.length + 1 : pokemons.data.length}
                    ref={ref}
                    itemSize={123}>
                      {ListItem}
                  </FixedSizeList>
                }}
              </InfiniteLoader>
        }
      </div>
  )
};

export default Home;
