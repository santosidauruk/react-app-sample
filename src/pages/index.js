import Home from './Home';
import Detail from './Detail';
import Filter from './Filter';

export {
  Home,
  Detail,
  Filter
}
