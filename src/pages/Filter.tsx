import React, { useEffect } from 'react';
import { Dropdown, Loader, Card } from 'semantic-ui-react';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { FixedSizeList } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { fetchFilteredPokemon } from '../redux/actions';
import usePokemonType from '../utils/usePokemonType';
import { RootState } from '../interfaces';

const Filter: React.FC = () => {
  const filteredPokemons = useSelector((state: RootState) => state.filteredList)
  const types = usePokemonType()
  const match = useRouteMatch<{ pokemonType: string}>()
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(() => {
    dispatch(fetchFilteredPokemon(match.params.pokemonType))
  }, [dispatch, match.params, match.params.pokemonType])

  const PokemonItem: React.FC<{index: number, style: {}}> = ({ index, style }) => {
    return (
      <Card centered style={{
          ...style,
          margin: '0 0 14px 0'
        }}>
        <Card.Content>
          <Card.Header style={{
            marginTop: 0
          }}>{filteredPokemons.data[index].pokemon.name}</Card.Header>
        </Card.Content>
      </Card>
    )
  }

  return (
    <div style={{
      height: '100vh'
    }}>
      <Dropdown
          style={{
            width: '200px',
            margin: '0 auto 40px auto'
          }}
          placeholder='Select Type'
          defaultValue={match.params.pokemonType}
          fluid
          search
          selection
          options={types}
          onChange={(_, data) => history.push(`/pokemon/type/${data.value}`)}
        />
        {
          filteredPokemons.loading
          ? <Loader active inline="centered"/>
          : <AutoSizer>
              {({ height, width }) => (
              <FixedSizeList
                height={height}
                itemCount={filteredPokemons.data.length}
                itemSize={51}
                width={width}
              >
                {PokemonItem}
                </FixedSizeList>
            )}
          </AutoSizer>
        }
    </div>
  );
};

export default Filter;
