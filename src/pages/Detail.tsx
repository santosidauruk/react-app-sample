import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { Loader } from 'semantic-ui-react';
import isEmpty from '../utils/isEmpty';
import getPokemon from '../services/getPokemon';
import PokeDetail from '../components/PokeDetail';
import { PokemonDetail } from '../interfaces';

const Detail: React.FC = () => {
  const match = useRouteMatch<{name: string}>()
  const [pokemonDetail, setPokemonDetail] = useState<PokemonDetail>({
    sprites: {},
    abilities: [],
    types: []
  })

  useEffect(() => {
      getPokemon({
        path: `/pokemon/${match.params.name}`
      })
        .then(pokemon => {
          setPokemonDetail(pokemon)
        })
        .catch(error => { throw error })
  }, [match.params, match.params.name])

  return (
    <div>
      {
        isEmpty(pokemonDetail)
          ? <Loader active inline="centered"/>
          : <PokeDetail detail={pokemonDetail}/>
      }
    </div>
  );
};

export default Detail;
