import mockAxios from 'axios'
import getPokemonOfType from '../getPokemonOfType';

const spyError = jest.spyOn( console, 'error' )

describe('get pokemons with given type', () => {
  it('successfuly fetch data from API', async () => {
    const fakePokemonData = [
      {
        "pokemon": {
          "name": "pidgey",
          "url": "https://pokeapi.co/api/v2/pokemon/16/"
        },
        "slot": 1
      },
      {
        "pokemon": {
          "name": "pidgeotto",
          "url": "https://pokeapi.co/api/v2/pokemon/17/"
        },
        "slot": 1
      }
    ]

    mockAxios.get.mockImplementationOnce(() => {
      return Promise.resolve({
        data: {
          pokemon: fakePokemonData
        }
      })
    })
    const response = await getPokemonOfType()
    expect(response).toEqual(fakePokemonData)
    expect(mockAxios.create).toHaveBeenCalledTimes(1);
    expect(spyError).not.toHaveBeenCalled()
  })

  it('error fetch data from API', async () => {
    mockAxios.get.mockImplementationOnce(() => {
      return Promise.reject('error fetch')
    })

    getPokemonOfType()
      .catch(error => {
        expect(error).toEqual('error fetch')
      })
  })
})
