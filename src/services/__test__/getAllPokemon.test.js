import mockAxios from 'axios'
import getAllPokemon from '../getAllPokemon';

const spyError = jest.spyOn( console, 'error' )

describe('get All Pokemon', () => {
  it('successfuly fetch data from API', async () => {
    const fakePokemonData = {
      next: "https://pokeapi.co/api/v2/pokemon/?offset=20&limit=20",
      results: [
        {
          name: "bulbasaur",
          url: "https://pokeapi.co/api/v2/pokemon/1/"
        },
        {
          name: "ivysaur",
          url: "https://pokeapi.co/api/v2/pokemon/2/"
        },
        {
          name: "venusaur",
          url: "https://pokeapi.co/api/v2/pokemon/3/"
        },
      ]
    }

    mockAxios.get.mockImplementationOnce(() => {
      return Promise.resolve({
        data: fakePokemonData
      })
    })
    const response = await getAllPokemon()
    expect(response).toEqual(fakePokemonData)
    expect(mockAxios.create).toHaveBeenCalledTimes(1);
    expect(spyError).not.toHaveBeenCalled()
  })

  it('error fetch data from API', async () => {
    mockAxios.get.mockImplementationOnce(() => {
      return Promise.reject('error fetch')
    })

    getAllPokemon()
      .catch(error => {
        expect(error).toEqual('error fetch')
      })
  })
})
