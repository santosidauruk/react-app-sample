import mockAxios from 'axios'
import getPokemon from '../getPokemon';

const spyError = jest.spyOn( console, 'error' )

describe('get One Pokemon', () => {
  it('successfuly fetch data from API', async () => {
    const fakePokemonData = {
      abilities: [
        {
          ability: {
            name: "chlorophyll",
            url: "https://pokeapi.co/api/v2/ability/34/"
          },
          is_hidden: true,
          slot: 3
        }
      ],
      base_experience: 64,
    }

    mockAxios.get.mockImplementationOnce(() => {
      return Promise.resolve({
        data: fakePokemonData
      })
    })
    const response = await getPokemon()
    expect(response).toEqual(fakePokemonData)
    expect(mockAxios.create).toHaveBeenCalledTimes(1);
    expect(spyError).not.toHaveBeenCalled()
  })

  it('error fetch data from API', async () => {
    mockAxios.get.mockImplementationOnce(() => {
      return Promise.reject('error fetch')
    })

    getPokemon()
      .catch(error => {
        expect(error).toEqual('error fetch')
      })
  })
})
