import mockAxios from 'axios'
import getType from '../getType';

const spyError = jest.spyOn( console, 'error' )

describe('get all types of pokemon', () => {
  it('successfuly fetch data from API', async () => {
    const fakeTypeData = [
      {
        "name": "lovely-kiss",
        "url": "https://pokeapi.co/api/v2/move/142/"
      },
      {
        "name": "transform",
        "url": "https://pokeapi.co/api/v2/move/144/"
      },
      {
        "name": "dizzy-punch",
        "url": "https://pokeapi.co/api/v2/move/146/"
      }
    ]

    mockAxios.get.mockImplementationOnce(() => {
      return Promise.resolve({
        data: {
          results: fakeTypeData
        }
      })
    })
    const response = await getType()
    expect(response).toEqual(fakeTypeData)
    expect(mockAxios.create).toHaveBeenCalledTimes(1);
    expect(spyError).not.toHaveBeenCalled()
  })

  it('error fetch data from API', async () => {
    mockAxios.get.mockImplementationOnce(() => {
      return Promise.reject('error fetch')
    })

    getType()
      .catch(error => {
        expect(error).toEqual('error fetch')
      })
  })
})
