import { stringify } from 'query-string'
import { axiosInstance } from '../utils/axios';
import { Pokemons } from '../interfaces';

const getAllPokemon = ({
  path = '',
  param = {}
} = {}): Promise<Pokemons> => {
  const queryParam = stringify(param) ? `?${stringify(param)}` : ''
  const url = `${path}${queryParam}`
  return axiosInstance.get(url)
    .then(response => response.data)
    .catch(error => { throw error })
}

export default getAllPokemon
