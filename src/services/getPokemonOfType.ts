import { axiosInstance } from '../utils/axios';
import { endpointPath } from '../constants/api';

const getPokemonOfType = ({
  path = endpointPath.type,
  param = {
    type: ''
  }
} = {}): Promise<{}> => {
  return axiosInstance.get(`${path}/${param.type}`)
    .then(response => {
      return response.data.pokemon
    })
    .catch(error => { throw error })
}

export default getPokemonOfType
