import { axiosInstance } from '../utils/axios';
import { endpointPath } from '../constants/api';

const getType = ({
  path = endpointPath.type,
  param = {}
} = {}): Promise<{}> => {

  return axiosInstance.get(path)
    .then(response => {
      return response.data.results
    })
    .catch(error => { throw error })
}

export default getType
