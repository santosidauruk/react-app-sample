import { axiosInstance } from '../utils/axios';
import { PokemonDetail } from '../interfaces';

const getPokemon = ({
  path = '',
  param = {}
} = {}): Promise<PokemonDetail> => {

  return axiosInstance.get(path)
    .then(response => {
      return response.data
    })
    .catch(error => { throw error })
}

export default getPokemon
